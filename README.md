nix-genpkg
====

Description:
------------
This is a script to automatically generate a nix expression that can be used for
building a Nix package. It basically precomputes the src SHA256 depending on the
chosen fetcher using `nix-prefetch` and prints the corresponding nix expression 
on the screen.

Installation:
-------------
Copy the python script somewhere in the `PATH` or use installation script `install.py`.
See `./install.py --help`.

Usage:
------
See `nix-genpkg.py --help`.

#!/usr/bin/env python3

"""
This script depends on nix-prefetch to compute the sha256 hash
"""
from enum import Enum, auto
import argparse
import shutil
import subprocess
import sys


class SupportedLanguages(Enum):
    # the associated value is the name of the derivation function
    C = "stdenv.mkDerivation"
    RUST = "rustPlatform.buildRustPackage"


class SupportedFetchers(Enum):
    GIT = "fetchgit"
    GITHUB = "fetchFromGitHub"
    GITLAB = "fetchFromGitLab"


class Fetcher:
    """
    Representation of a Nixpkgs fetcher
    """

    def __init__(self, fetcher_type, **kargs):
        if not isinstance(fetcher_type, SupportedFetchers):
            raise ValueError()
        self.fetcher = fetcher_type
        self.args = kargs
        # if fetcher_type == SupportedFetchers.GIT:
        #    print("{}".format(fetcher_type.value))
        # elif fetcher_type == SupportedFetchers.GITHUB:
        #    print("{}".format(fetcher_type.value))
        # elif fetcher_type == SupportedFetchers.GITLAB:
        #    print("{}".format(fetcher_type.value))

    def compute_sha256(self):
        """
        returns the sha256 value to use in the fetcher
        """
        repo = self.args["name"] if self.args["repo_from_name"] else self.args["repo"]
        command = ["nix-prefetch", self.fetcher.value]
        if self.fetcher == SupportedFetchers.GIT:
            command += [
                "--url",
                self.args["url"],
            ]
        else:
            # This is the same for github/gitlab fetchers
            command += [
                "--owner",
                self.args["owner"],
                "--repo",
                repo,
            ]
        if self.args["rev"] is not None:
            command += [
                "--rev",
                self.args["rev"].replace("${version}", self.args["version"]),
            ]
        out = subprocess.run(command, capture_output=True, text=True)
        for line in out.stdout.split("\n"):
            if line.find("sha256-") == 0:
                # print(f"DBG: SHA256={line}")
                return line
        return None

    def getfetcher(self):
        """
        returns the nix name of the fetcher for usage in a nix expression
        """
        return self.fetcher.value

    def text(self):
        """
        returns the nix expression calling the fetcher
        """
        text = '{fetcher} {{ {specific}{rev}sha256 = "{h}"; }}'.format(
            fetcher=self.fetcher.value,
            specific=(
                'url = "{url}"; '.format(**self.args)
                if self.fetcher == SupportedFetchers.GIT
                else 'owner = "{owner}"; repo = "{repo}";'.format(**self.args)
            ),
            rev=(
                "rev = {}; ".format(self.args["rev"])
                if self.args["rev"] is not None
                else ""
            ),
            h=self.compute_sha256(),
        )
        # print(f"DBG:\n{text}")
        return text


class Builder:
    """
    Represents a derivation builder object
    """

    pass


class PackageGenerator:
    """
    Contains all the necessary input to generate a package file
    """

    def __init__(self, fetcher, language, **kwargs):
        self.fetcher = Fetcher(fetcher, **kwargs)
        self.language = language
        self.args = kwargs

    def generate_package(self):
        """
        prints the content of the package file as a nix function on stdout
        """
        arg_line = "{{ lib, {fetcher}, {platform} }}:".format(
            fetcher=self.fetcher.getfetcher(),
            platform=self.language.value.split(".")[0],
        )
        body = """ 

let 
  pkgs = import <nixpkgs> {{}};
in {derivation_function} rec {{
  pname = "{name}";
  version = "{version}";

  src = {fetcher};

  meta =  with lib; {{
    description = "{description}";
    license = licenses.{license};
    platforms = platforms.all;
  }};
  {buildinputs}

  {extra}
}}
            """.format(
            derivation_function=self.language.value,
            fetcher=self.fetcher.text(),
            extra=(
                'cargoSha256 = "";' if self.language == SupportedLanguages.RUST else ""
            ),
            buildinputs=(
                "buildInputs = [ {} ];".format(
                    ", ".join(map(lambda s: f"pkgs.{s}", self.args["build_inputs"]))
                )
                if self.args["build_inputs"] is not None
                else ""
            ),
            **self.args,
        )
        print(
            f""" 
{arg_line}
{body}
            """
        )


if __name__ == "__main__":

    # ensure that nix-prefetch is installed
    if shutil.which("nix-prefetch") is None:
        sys.exit("nix-prefetch not found in $PATH")

    class FetcherAction(argparse.Action):
        def __init__(self, option_strings, dest, **kwargs):
            super().__init__(option_strings, dest, **kwargs)

        # def __call__(self, parser, namespace, values):
        def __call__(self, parser, namespace, values, option_string=None):
            setattr(namespace, self.dest, SupportedFetchers[values.upper()])

    class LanguageAction(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            setattr(namespace, self.dest, SupportedLanguages[values.upper()])

    class RevFromTagAction(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            setattr(namespace, self.dest, True)
            setattr(namespace, "rev", "refs/tags/v${version}")

    class RepoFromNameAction(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            setattr(namespace, self.dest, True)
            setattr(namespace, "repo", "${pname}")

    parser = argparse.ArgumentParser(
        description="A package generator for Nixpkgs",
        epilog="Supported fetchers: {}\nSupported languages: {}".format(
            ", ".join(map(lambda f: f.name.lower(), list(SupportedFetchers))),
            ", ".join(map(lambda l: l.name.lower(), list(SupportedLanguages))),
        ),
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "--name",
        "-n",
        required=True,
        help="the pname value of the package (package name)",
    )
    parser.add_argument(
        "--version", "-v", required=True, help="the version of the package"
    )
    parser.add_argument(
        "--fetcher",
        "-f",
        required=True,
        # type=SupportedFetchers,
        action=FetcherAction,
        help="the fetcher type",
    )
    parser.add_argument(
        "--language",
        "-l",
        required=False,
        # type=SupportedLanguages,
        action=LanguageAction,
        default=SupportedLanguages.C,
        help="the language the program was written",
    )
    parser.add_argument(
        "--description", "-d", default="", help="a one line description of the package"
    )
    parser.add_argument(
        "--license",
        "-L",
        default="gpl3",
        help="the license string of the package (default: %(default)s)",
    )
    parser.add_argument(
        "--build-inputs",
        metavar="DEPENDENCY",
        required=False,
        action="append",
        help="build/runtime dependency (can be specified multiple times)",
    )
    fetcher_args = parser.add_argument_group("extra fetcher arguments")
    git_args = fetcher_args.add_mutually_exclusive_group()
    # git_args
    # fetcher_args.add_argument("--url", help="the url of the git repo")
    # fetcher_args.add_argument("--owner", help="the owner of the repo")
    # fetcher_args.add_argument("--repo", help="the name of the repo")
    git_args.add_argument("--url", help="the url of the git repo")
    git_args.add_argument("--owner", help="the owner of the repo")
    repo_args = fetcher_args.add_mutually_exclusive_group()
    repo_args.add_argument("--repo", help="the name of the repo")
    repo_args.add_argument(
        "--repo-from-name",
        help="use value provided for --name as repo",
        action=RepoFromNameAction,
        nargs=0,
    )
    rev_args = fetcher_args.add_mutually_exclusive_group(required=False)
    rev_args.add_argument("--rev", help="the revision to work with")
    rev_args.add_argument(
        "--rev-tag-from-version",
        help="build the rev using version based tag",
        action=RevFromTagAction,
        nargs=0,
    )

    args = parser.parse_args()
    # print(f"DBG {args=}")
    # check that fetcher has the required args
    if args.fetcher == SupportedFetchers.GIT:
        assert args.url is not None
    else:
        assert args.owner is not None
        assert args.repo is not None

    PackageGenerator(**vars(args)).generate_package()

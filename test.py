#!/usr/bin/env python3

import unittest
from nix_genpkg import Fetcher, SupportedFetchers


class FetcherTest(unittest.TestCase):
    def test_compute_sha256(self):
        f = Fetcher(
            SupportedFetchers.GIT,
            url="https://framagit.org/rogarb/rnotes",
            rev="refs/tags/v0.2.0",
        )
        self.assertEqual(
            f.compute_sha256(), "sha256-379wLRq7Iz9+RKplADKpictHr36kMf+N4TMPjFAEdvY="
        )
        f = Fetcher(
            SupportedFetchers.GITHUB,
            owner="rogarb",
            repo="huextract",
            version="",
            rev="refs/tags/v0.2.1",
        )
        self.assertEqual(
            f.compute_sha256(), "sha256-0ZEAdo3gQhyrJEe0T6BCedrsJTXP53kyijUbTFM0vCY="
        )

    def test_text(self):
        f = Fetcher(
            SupportedFetchers.GIT,
            url="https://framagit.org/rogarb/rnotes",
            rev="refs/tags/v0.2.0",
        )
        self.assertEqual(
            f.text(),
            'fetchgit { url = "https://framagit.org/rogarb/rnotes"; rev = "refs/tags/v0.2.0"; sha256 = "sha256-379wLRq7Iz9+RKplADKpictHr36kMf+N4TMPjFAEdvY="; }',
        )


if __name__ == "__main__":
    unittest.main()
